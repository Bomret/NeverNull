﻿namespace NeverNull {
    /// <summary>
    ///     Represents the absence of a value.
    /// </summary>
    public struct None {
        /// <summary>
        ///     Returns the string representation of this type.
        /// </summary>
        /// <returns></returns>
        public override string ToString() => "None";
    }
}