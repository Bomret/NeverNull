### 3.2.0 - December 27 2015
* Match, IfSome and IfNone methods are implemented directly in Option<T> 
* Deprecated TryGet in favor of Match, IfSome and IfNone
* Deprecated GetOrDefault because it might reintroduce null
* Added Predef module for C# 6 static imports
* Fixed a bug with AggregateOptional and AggregateOptionalNullable [Issue #6](https://github.com/Bomret/NeverNull/issues/6)

### 3.1.0 - December 12 2015
* Additional lazy overloads for GetOrElse and OrElse
* Additional overloads and methods for working with nullables

### 3.0.1 - December 8 2015
* Fixed broken Nuget package

### 3.0.0 - December 7 2015
* Complete rewrite

#### 2.0.0.14 - August 25 2014
* Last legacy build

